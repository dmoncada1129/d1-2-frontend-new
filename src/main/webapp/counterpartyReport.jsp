
<jsp:useBean id="globalHelper" class="deutschebank.core.ApplicationScopeHelper" scope="application"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <%--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--%>
        <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>--%>

        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <%--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--%>

        <%--<script--%>
            <%--src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">--%>
        <%--</script>--%>


        <%--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--%>


        <title>DB Counterparty Table </title>


    </head>
    
    <body>
    <%--<table data-pagination="true" data-search="true" data-toggle="table" data-url="deal_array.json">--%>
    <table data-pagination="true" data-search="true" data-toggle="table" data-show-refresh="true" data-show-toggle="true"
           data-show-columns="true" data-url="http://10.11.32.20:81/dbanalyzer/rws/services/report/counterparty/">
        <thead>
        <tr>
            <th data-sortable="true" data-field="counterpartyName">Counterparty Name</th>
            <th data-sortable="true" data-field="counterpartyStatus">Counterparty Status</th>
            <th data-sortable="true" data-field="counterpartyDate">Counterparty Date Registered</th>

        </tr>
        </thead>
    </table>


    <%--&lt;%&ndash;<div id="toolbar">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<button id="remove" class="btn btn-danger" disabled>&ndash;%&gt;--%>
            <%--&lt;%&ndash;<i class="glyphicon glyphicon-remove"></i> Delete&ndash;%&gt;--%>
        <%--&lt;%&ndash;</button>&ndash;%&gt;--%>
    <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
    <%--<div id="toolbar" class="btn-group">--%>
        <%--<button type="button" class="btn btn-default">--%>
            <%--<i class="glyphicon glyphicon-plus"></i>--%>
        <%--</button>--%>
        <%--<button type="button" class="btn btn-default">--%>
            <%--<i class="glyphicon glyphicon-heart"></i>--%>
        <%--</button>--%>
        <%--<button type="button" class="btn btn-default">--%>
            <%--<i class="glyphicon glyphicon-trash"></i>--%>
        <%--</button>--%>
    <%--</div>--%>

    <%--<table id="table"--%>
           <%--data-toolbar="#toolbar"--%>
           <%--data-locale="en-US"--%>
           <%--data-sortable="true"--%>
           <%--data-search="true"--%>
           <%--data-show-refresh="true"--%>
           <%--data-show-toggle="true"--%>
           <%--data-show-columns="true"--%>
           <%--data-show-export="true"--%>
           <%--data-detail-view="true"--%>
           <%--data-detail-formatter="detailFormatter"--%>
           <%--data-minimum-count-columns="2"--%>
           <%--data-show-pagination-switch="true"--%>
           <%--data-pagination="true"--%>
           <%--data-id-field="dealID"--%>
           <%--data-page-list="[10, 25, 50, 100, ALL]"--%>
           <%--data-side-pagination="server"--%>
           <%--data-url="deal_array.json"--%>
           <%--data-response-handler="responseHandler">--%>
    <%--</table>--%>


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous">
    </script>


    <%--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--%>



    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">


    <!-- Latest compiled and minified JavaScript -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>


    <!-- Latest compiled and minified Locales -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-en-US.min.js"></script>
    <%--<script src="bootstrap-table-en-US.js"></script>--%>



    <script src="dbanalyzer/js/counterpartyReport.js"></script>


    </body>
</html>

